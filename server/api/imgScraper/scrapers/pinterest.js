'use strict';

var request = require('request');
var cheerio = require('cheerio');

exports.list = function(url, cb) {
    request(url, function(error, resp, body) {
        if(error) {
           cb({
               error:error
           }); 
        }
        if(!error) {
            // load cheerio object, standard
            var $ = cheerio.load(body);
            var pin = {};

            // url of a pin that the userr has given us
            var $url = url;
            //var $url = 'http://magicseaweed.com/photoLab/potd/';
            // pull out specified selectors
            // select the div class of a pinterest page that contains an image
            var $img = $('.heightContainer img').attr('src'); 
            //var $img = $('.insetbar-lift img').attr('src'); 

            // select the div class of a pinterest page that contains the description of the image
            var $desc = $('.heightContainer img').attr('alt');
            //var $desc = $('.insetbar-lift img').attr('alt');

            console.log($img + ' pin url');

            var pin = {
                img: $img,
                url: $url,
                desc: $desc
            }
            // respond with the final JSON object add the pin object to the CallBack
            cb(pin);
        }
    });
}