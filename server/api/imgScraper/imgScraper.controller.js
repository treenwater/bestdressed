'use strict';

var fs = require('fs');
var request = require('request');
var cheerio =require('cheerio');

var scrapers = {};

scrapers['pinterest'] = require('./scrapers/pinterest.js');
//scrapers['surfer'] = require('./scrapers/surfer.js');
//scrapers['instagram'] = require('./scrapers/instagram.js');

exports.scrape = function(req, res)  {
    var url = req.body.url;
    var scraperToUse;

// if using more than one scraper add 'else if' and then url.indexOf('whatevahs')
    if(url.indexOf('pinterest') > -1) {
        scraperToUse = 'pinterest';
    }else{
        console.log('No Spock dat scraper brah.');
    }

    scrapers[scraperToUse].list(url, function(data) {
        console.log('data from scraper:', data);
        //return in json
        res.json(data);
    });
}